# Accelerator_nlp

## Intro 
NLP is a growing area of study, coming out of academia and into actual practical business applications in the last few years. 
Within the NHS and public sector organisations in general we generate a mountain of text data to the point that it really is not feasible to handle it by hand 
given the sizeable backlog.

An internal group sat down and brainstormed ideas to utilise this dearth of data. This project concerns itself with two of them:

1.  Text classification(SKlearn, fasttext, BERT)
2.  Topic modelling (textacy)


## Data Exploration
The first step with any data science project is to examine the data. Look at summary statistics and get a feel for it's limitations. 
For this piece of work two corpora were available. 

1.  A set of discharge advice letters from hospital admissions(DAL)
2.  The presenting complaint from the hospital IT system Emergency Department (EDDS)

### DAL
**Example**
"24 year old man referred by GP for CT scan. Has had several concussions over the last few years and has been struggling with 
focus and irritability since falling from ladder 2 weeks ago."

Positives:
- Better quality data
- No limit on string length
- Longer average length
- Good Capitalisation

Negatives:
- Cannot be linked to specialty label
- Fewer samples

### EDDS
**Example**
"FELL AT HOME INJ R ANKLE & HEAD"

Positives:
- Has specialty label
- Can be linked to other datasets
- More training samples

Negatives:
- Entire string is capitalised which confounds many algorithms.
- String gets truncated after 255 characters

In the end we decided to go with the DAL due to the higher quality data and because it is not truncated. 
Our plan to get around the lack of a training label was to generate them using unsupervised methods(Topic Modelling)

## Preprocessing
For Preprocessing I tried several approaches but in general I found more advanced techniques like stemming and lemmatization hurt performance.
As a result I kept preprocessing fairly basic; normalising whitespace, fixed garbled unicode, dropped null records and dropped duplicate records.

## Modelling - Topic Models
Topic modelling is a type of statistical model that seperates a corpus of documents into several topics. 

We hoped the terms would seperate cleanly into different topics:
e.g.

Topic 1: chest, pain, SOB, MI 
(Cardiology)

Topic 2: Broken, fracture, leg, arm, R,L
(Trauma & Orthopedics)

Topic 3: head, ache, blurry, dizziness
(Neurology)

This would allow us to train on the better quality dataset
### Actual Results

![alt text](https://gitlab.com/data-science-projects-Arthur/datasynth/uploads/a4c45f6a0540af5d21c8ed4352d8b981/download.png)

#### Good
Topic 9:
Terms: inj, fall, head, hip, rt, leg, lt, arm, l, side
possible label: Orthapedics

Topic 7:
Terms: chest, pain, sob, left, l, lt, sides, resus
Possible label: Cardiovascular

Topic 5:
Terms: injury, hits, left, hip, head, rt, back, liimb, right, arm
Possible Label: Orthapedics, Rheumatology

#### Bad
Topic 0:
terms: pain, number, hits, opportunity, bp, patient
Possible label: ???

most of the labels are like topic 0 implying they are not robust enough to use as training labels. As a result we had to pivot into using the lower quality dataset.

## Data Exploration EDDS

Since we have already examined the two corpus's the only remaining thing to study is the labels in the EDDS dataset.
![alt text](https://gitlab.com/data-science-projects-Arthur/datasynth/uploads/0f48fb626514d5753d33cc6ec4103a4c/Untitled.png)
There are 65 distinct labels of which 32 were removed due to low numbers (<1000) rows.

## Modelling - Text Classification
With the labels sorted two different models were trained on the dataset

**Model 1: Facebook FastText**
*  Word Vector Representation
    *  wikipedia pretrained vectors
*  Logistic Regression Classifier

**Model 2: Scikit Classifier**
*  tf-idf Representation
*  SGD Classifier(optimised logistic regression)

### Model Representation
Each of these approaches has a different representation with 
#### tf-idf

#### word embeddings

### Modelling results

## Conclusions and Future Work
In this project we've 